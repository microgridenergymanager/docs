Documentation for SEUP
======================

Simple Electric Utility Platform (SEUP) is an end-to-end platform for running and managing microgrids.

Contents:
^^^^^^^^^
.. toctree::
   :maxdepth: 3
    
   contact
   introduction
   devApps
   devAppsCloud
   devAppsHem
   troubleshoot
   contributors

.. Indices and tables
.. ==================

.. * :ref:`search`

.. * :ref:`genindex`
.. * :ref:`modindex`
